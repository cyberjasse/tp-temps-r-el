#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#define PORT 5555
#define IP "10.107.45.11"
#define MESSAGE "Insere ton message ici"

int sock;
int sockFils;

void stopper(){
	close(sock);
	close(sockFils);
	exit(0);
}

int main(int argc, char *argv[]){
	// pour CTRL-c
	struct sigaction vsa;
	vsa.sa_handler = stopper;
	sigemptyset( &vsa.sa_mask );
	vsa.sa_flags = 0;
	int result = sigaction( SIGINT, &vsa , NULL);
	if(result ==-1){
		perror("Erreur sigaction:");
		exit(-1);
	}
	//
	printf("start\n");
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock ==-1){
		perror("Erreur socket:");
		exit(-1);
	}
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(PORT);
	addr.sin_addr.s_addr = inet_addr(IP);
	printf("connection\n");
	result = connect(sock , (const struct sockaddr *)&addr , sizeof(struct sockaddr_in));
	if(result ==-1){
		perror("Erreur connect:");
		exit(-1);
	}
	printf("envoi\n");
	result = send(sock, MESSAGE, sizeof(MESSAGE), 0);
	if(result ==-1){
		perror("Erreur send:");
		exit(-1);
	}
	stopper();
	printf("FIN\n");
}
	
