#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#define PORT 5555
#define IP "127.0.0.1"
#define DATA 2

int sock; //numero de socket

void stopclient(){
	close(sock);
	exit(0);
}

int main(int argc, char *argv[]){
	// pour CTRL-c
	struct sigaction vsa;
	vsa.sa_handler = stopclient;
	sigemptyset( &vsa.sa_mask );
	vsa.sa_flags = 0;
	int result = sigaction( SIGINT, &vsa , NULL);
	if(result ==-1){
		perror("Erreur sigaction:");
		exit(-1);
	}
	//initialisation
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock ==-1){
		perror("Erreur socket:");
		exit(-1);
	}
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(PORT);
	addr.sin_addr.s_addr = inet_addr(IP);
	result = connect(sock , (const struct sockaddr *)&addr , sizeof(struct sockaddr_in));
	if(result ==-1){
		perror("Erreur connect:");
		exit(-1);
	}
	//envoi
	int data = DATA;
	result = send(sock, &data, sizeof(int), 0);
	if(result ==-1){
		perror("Erreur send:");
		exit(-1);
	}
	//reception
	result = recv(sock, &data, sizeof(int), 0);
	if(result ==-1){
		perror("Erreur send:");
		exit(-1);
	}
	printf("J'ai recu %d\n", data);
	close(sock);
}
