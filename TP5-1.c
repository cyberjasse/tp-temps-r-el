#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
//Pas oublier gcc tp.c -­o tp ­-pthread

void affiche(char *word){
	printf("%s\n", word);
}

int main(int argc, char *argv[]){
	if(argc < 2){
		printf("Il faut au moins 1 argument\n");
		exit(-1);
	}
	pthread_t idTable[argc-1];
	pthread_t tid = 0;
	int i;
	//on cree et lance les threads
	for(i=1 ; i<argc ; i++){
		int result = pthread_create(&tid, NULL, (void *)affiche, argv[i]);
		if(result != 0){
			perror("Erreur pthread_create:");
			exit(-1);
		}
		idTable[i-1] = tid;
	}
	//on attend la fin de tous les threads
	for(i=0 ; i<argc-1 ; i++){
		pthread_join(idTable[i], NULL);
	}
	printf("\n");
}
