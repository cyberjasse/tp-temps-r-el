#include <stdio.h>	// Necessaire pour printf()
#include <stdlib.h>	// Necessaire pour exit()

int main(int argc, char *argv[])
/*
argc est le nombre d'argument sachant que le premier argument est le nom du processus
argv est le tableau d'argument
*/
{
	if( argc < 2){
		printf("Il faut au moins 1 argument\n");
		exit(-1);
	}
	if( argc > 2){
		printf("Il faut un seul argument\n");
		exit(-1);
	}
	else{
		int param = atoi(argv[1]);
		printf("Le nombre de fils a cree est %d\n", param);
		int nbreFils=0;
		int id;
		while(nbreFils < param){
			id = fork();
			if(id > 0){//pere
				nbreFils++;
				//printf("%d Fils cree\n", nbreFils);
			}
			else if(id == 0){//fils
				printf("Je suis le fils %d\n", nbreFils +1);
				exit(0);
			}
			else{
				printf("Y a eu erreur lors de la creation du fils %d\n", nbreFils+1);
				//maintenant faut attendre tous les fils
				int i=0;
				for(i=0 ; i<nbreFils ; i++){
					wait(NULL);
				}
				exit(-1);
			}
		}
		//maintenant faut attendre tous les fils
		int i=0;
		for(i=0 ; i<nbreFils ; i++){
			wait(NULL);
		}
		exit(0);
	}
}
