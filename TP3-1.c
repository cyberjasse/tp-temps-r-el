#include <stdio.h>
#include <stdlib.h>
#include <sys/sem.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <signal.h>

int main(int argc, char *argv[]){
	//creation semaphore
	int semId = semget(IPC_PRIVATE, 1, 0777);
	semctl( semId , 0, SETVAL, 1);
	// pour CTRL-c
	void stopper(){
		printf("Arret presque propre\n");
		semctl( semId , 0, IPC_RMID, 0);
		exit(-1);
	}
	struct sigaction vsa;
	vsa.sa_handler = stopper;
	sigemptyset( &vsa.sa_mask );
	vsa.sa_flags = 0;
	int result = sigaction( SIGINT, &vsa , NULL);
	//implementation des fils
	void fils(){
		int pid = getpid();
		int i=0;
		struct sembuf sb = {0,-1,0};//veut dire "augmente le semaphore 0 de -1"
		int result = semop( semId, &sb, 1);
		for(i=0 ; i<3 ; i++){
			printf("Je suis le fils %d \n", pid);
			sleep(1);
		}
		sb.sem_op = +1;
		result = semop( semId, &sb, 1);
		exit(0);
	}
	//implementation du corps du programme
	int id = fork();
	if( id < 0){
		perror("Erreur premier fork:");
		semctl( semId , 0, IPC_RMID, 0);
		exit(-1);
	}
	
	else if( id == 0){// fils 1
		fils();
	}
	
	else{
		id = fork();
		if( id < 0){
			perror("Erreur deuxieme fork:");
			semctl( semId , 0, IPC_RMID, 0);
			exit(-1);
		}
		
		else if( id == 0){//fils 2
			fils();
		}
		
		else{//pere
			wait(NULL);
			wait(NULL);
			semctl( semId , 0, IPC_RMID, 0);
		}
	}
}
