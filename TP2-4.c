#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

int main(int argc, char *argv[]){

	void fils(int out, char *name){
		int sum = 0;
		int result;
		int data = 0;
		void lecture(){
			result = read(out , &data, sizeof(int));
			if(result < 0){
				printf("problem dans fils");
				exit(-1);
			}
			sum = sum + data;
		}
		struct sigaction lsa;
		lsa.sa_handler = lecture;
		sigemptyset( &lsa.sa_mask );
		lsa.sa_flags = 0;
		result = sigaction( SIGUSR1 , &lsa , NULL);
		if(result == -1){
			perror("erreur sigaction lecture\n");
			exit(-1);
		}
		
		void fermeture(){
			printf("Je suis le fils %s, la somme est %d, aurevoir.\n", name, sum);
			exit(0);
		}
		struct sigaction fsa;
		fsa.sa_handler = fermeture;
		sigemptyset( &fsa.sa_mask );
		fsa.sa_flags = 0;
		result = sigaction( SIGUSR2 , &fsa , NULL);
		if(result == -1){
			perror("erreur sigaction fermeture\n");
			exit(-1);
		}
		while(1)
			pause();
	}
	
	int p[2];
	int result = pipe(p);
	if(result<0){ exit(-1); }
	int in = p[1];
	int out = p[0];
	
	int idG = fork();
	if(idG == 0){//je suis le fils gauche
		close(in);//?
		fils( out , "gauche");
	}
	else if(idG < 0){
		perror("Erreur au 1er fork\n");
		exit(-1);
	}
	else{ //je suis le pere
		int idD = fork();
		if(idD == 0){//je suis le fils droite
			close(in);//?
			fils( out , "droite");
		}
		else if(idD < 0){
			perror("Erreur au 2eme fork\n");
			exit(-1);
		}
		else{ //je suis le pere
			int data = 1;
			while( data != 0){
				printf("Entrez un entier\n");
				scanf("%d", &data);
				result = write(in, &data, sizeof(int) );
				if( data > 0){
					kill( idD , SIGUSR1);
				}
				else if( data < 0){
					kill( idG , SIGUSR1);
				}
			}
			kill( idG , SIGUSR2);
			kill( idD , SIGUSR2);
			wait(NULL);
			wait(NULL);
			close(in);
			close(out);
		}
	}
}
