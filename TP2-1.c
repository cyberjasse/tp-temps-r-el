#include <stdio.h>
#include <stdlib.h>
#include <signal.h>

int main(int argc, char *argv[]){
	// ignorer CTRL-c
	struct sigaction vsa;
	vsa.sa_handler = SIG_IGN;
	sigemptyset( &vsa.sa_mask );
	vsa.sa_flags = 0;
	int result = sigaction( SIGINT, &vsa , NULL);
	// reagir a l'alarme
	void alarmReact(){
		printf(" --- 5 --- \n");
		alarm(5);
	}
	//signal(SIGALRM , alarmReact);
	struct sigaction asa;
	asa.sa_handler = alarmReact;
	asa.sa_mask = vsa.sa_mask; //sigemptyset( &asa.sa_mask );
	asa.sa_flags = 0;
	result = sigaction( SIGALRM , &asa , NULL);
	if(result == -1){
		perror("sigaction");
		exit(-1);
	}
	 // ca marche pas avec sigaction
	alarm(5);
	while(1){
		printf("top\n");
		sleep(1);
	}
}
