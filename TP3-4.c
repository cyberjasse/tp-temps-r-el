#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/msg.h>
#include <sys/types.h>
#include <sys/ipc.h>

int mesId;

struct msgbuf{
	long mtype;
	int donnee;
};

int stopper(int retour){
	printf("Stop\n");
	msgctl(mesId, IPC_RMID, NULL);
	exit(retour);
}

void sigintReact(){
	stopper(-1);
}

int fils(int id){
	int data = 1;
	int sum = 0;
	int result;
	struct msgbuf mb;
	while(data != 0){
		result = msgrcv(mesId , &mb, sizeof(int), id, 0);
		data = mb.donnee;
		//printf("%d recoit %d\n", id, data);
		sum = sum + data;
		//printf("la somme de %d est %d\n", id, sum);
	}
	printf("Je suis fils %d ma somme est %d\n", id, sum);
	stopper(0);
}
		
int main(int argc, char *argv[]){
	//creation file de message
	mesId = msgget(IPC_PRIVATE , 0777);
	// pour CTRL-c
	struct sigaction vsa;
	vsa.sa_handler = sigintReact;
	sigemptyset( &vsa.sa_mask );
	vsa.sa_flags = 0;
	int result = sigaction( SIGINT, &vsa , NULL);
	//corps du programme
	int id = fork();
	if( id < 0){
		perror("Erreur 1er fork:");
		stopper(-1);
	}
	else if( id ==0){//fils de gauche
		fils(1);
	}
	else{
		int id = fork();
		if( id < 0){
			perror("Erreur 2e fork:");
			stopper(-1);
		}
		else if( id ==0){//fils de gauche
			fils(2);
		}
		else{// pere
			struct msgbuf mb;
			mb.donnee = 1;
			while( mb.donnee != 0){
				//entree de la donnee
				printf("Entrez un entier\n");
				scanf("%d", &mb.donnee);
				//creation du courrier
				if(mb.donnee < 0)
					mb.mtype = 1;
				else
					mb.mtype = 2;
				//envoi courrier
				result = msgsnd(mesId, &mb, sizeof(int), 0);
				if(result < 0){
					perror("l'envoi du message n'est pas accepte.");
					stopper(-1);
				}
				//gerer le cas ou data=0
				if(mb.donnee == 0){
					mb.mtype = 1;
					result = msgsnd(mesId, &mb, sizeof(int), 0);
					if(result < 0){
						perror("l'envoi du 0 n'est pas accepte.");
						stopper(-1);
					}
				}
			}
			wait(NULL);
			wait(NULL);
			stopper(0);
		}
	}
}
