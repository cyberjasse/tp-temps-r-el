#include <stdio.h>	// Necessaire pour printf()
#include <stdlib.h>	// Necessaire pour exit()
//#include <unistd.h>	// Necessaire pour execl()
//#include <signal.h>

int main(int argc, char *argv[])
{
	int id = fork();
	if( id == 0)//Je suis le fils
	{
		int i=0;
		for(i=0 ; i<5 ; i++){
			printf("Je suis le fils\n");
			sleep(1);
		}
		exit(0);
	}
	else if( id > 0)//Je suis le pere
	{
		int i=0;
		for(i=0 ; i<3 ; i++){
			printf("Je suis le pere\n");
			sleep(1);
		}
		//int idFils = wait();//Modification : attendre mort du fils
		printf("Le fils est mort. \n");
		exit(0);
	}
	else//problem
	{
		printf(" y a problem, l'identifiant du processus est %d\n", id);
		exit(-1);
	}
	exit(0);
}
