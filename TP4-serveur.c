#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <signal.h>
#define MAXCLIENT 3
#define PORT 5555
#define IP "127.0.0.1"

int sock; //numero de socket principal
int sockFils;//numero de socket fils

void stopserv(){
	close(sock);
	exit(0);
}

void stopfils(){
	close(sockFils);
	exit(0);
}

int main(int argc, char *argv[]){
	// ON IGNORE LES SIGCHILD car on attend pas les codes de retours (pas de wait)
	signal(SIGCHLD, SIG_IGN);
	// pour CTRL-c
	struct sigaction vsa;
	vsa.sa_handler = stopserv;
	sigemptyset( &vsa.sa_mask );
	vsa.sa_flags = 0;
	int result = sigaction( SIGINT, &vsa , NULL);
	if(result ==-1){
		perror("Erreur sigaction:");
		exit(-1);
	}
	//initialisation
	sock = socket(AF_INET, SOCK_STREAM, 0);
	if(sock ==-1){
		perror("Erreur socket:");
		exit(-1);
	}
	struct sockaddr_in addr;
	addr.sin_family = AF_INET;
	addr.sin_port = htons(PORT);
	addr.sin_addr.s_addr = inet_addr(IP);
	result = bind(sock , (const struct sockaddr *)&addr , sizeof(struct sockaddr_in));
	if(result ==-1){
		perror("Erreur bind:");
		close(sock);
		exit(-1);
	}
	result = listen(sock, MAXCLIENT);
	if(result ==-1){
		perror("Erreur listen:");
		close(sock);
		exit(-1);
	}
	printf("serveur ON\n");
	while(1){
		struct sockaddr sa;
		socklen_t sl;
		sockFils = accept(sock, &sa, &sl);
		if(result ==-1){
			perror("Erreur accept:");
			close(sock);
			exit(-1);
		 }
		 int id = fork();
		 if(id < 0){
		 	perror("Erreur fork:");
		 	close(sock);
		 	close(sockFils);
		 	exit(-1);
		 }
		 else if(id > 0){
		 	printf("connection etablie\n");
		 }
		 else{
		 	// pour CTRL-c
			struct sigaction vsa;
			vsa.sa_handler = stopfils;
			sigemptyset( &vsa.sa_mask );
			vsa.sa_flags = 0;
			int result = sigaction( SIGINT, &vsa , NULL);
			if(result ==-1){
				perror("Erreur sigaction fils:");
				exit(-1);
			}
		 	int data;
		 	//recevoir
		 	result = recv(sockFils, &data, sizeof(int), 0);
		 	if(result == -1){
		 		perror("Erreur recv:");
		 		close(sockFils);
		 		exit(-1);
		 	}
		 	//traiter
		 	data = data*data;
		 	//envoier
		 	result = send(sockFils, &data, sizeof(int), 0);
		 	printf("client satisfait\n");
		 	exit(0);
		 }	
	}
}
