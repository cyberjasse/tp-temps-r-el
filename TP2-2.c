#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/time.h>//pour utiliser setitimer et getitimer

float reste = 0;

int main(int argc, char *argv[]){
	if( argc < 2){
		printf("Il faut au moins 1 argument\n");
		exit(-1);
	}
	if( argc > 2){
		printf("Il faut un seul argument\n");
		exit(-1);
	}
	int param = atoi(argv[1]);
	reste = param;
	//reagir a l'alarme
	void alarmReact(){
		reste = reste - 0.1f;
		printf("Il reste %.1f secondes\n", reste);
		if(reste <= 0.0001f) exit(0);
	}
	struct sigaction asa;
	asa.sa_handler = alarmReact;
	sigemptyset( &asa.sa_mask );
	asa.sa_flags = 0;
	int result = sigaction( SIGALRM , &asa , NULL);
	if(result == -1){
		perror("erreur sigaction\n");
		exit(-1);
	}
	//

	struct timeval v;
	v.tv_sec = 0;
	v.tv_usec = 10000;
	
	struct itimerval timeval;
	timeval.it_interval = v;
	timeval.it_value = v;
	
	printf("Demarrage timer a %d secondes\n", param);
	setitimer( ITIMER_REAL , &timeval , NULL);
	//boucle infinie
	while(1)
	pause();
}
