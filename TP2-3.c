#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <sys/time.h>//pour utiliser setitimer et getitimer

int main(int argc, char *argv[]){
	int id = fork();
	if(id == 0){//je suis le fils
		char data = 'n';
		while(data != 'y'){
			printf("Voulez-vous que j'envoie un signal au pere ? (y/n)\n");
			scanf("%c", &data);
			getchar();
		}
		kill( getppid() , SIGUSR1);
		pause();
	}
	else if(id < 0){
		perror("Erreur au fork\n");
		exit(-1);
	}
	else{ //je suis le pere
		void infanticide(){
			printf("J'ai recu un signal. je vais le tuer\n");
			kill( id , SIGKILL);
		}
		struct sigaction sa;
		sa.sa_handler = infanticide;
		sigemptyset( &sa.sa_mask );
		sa.sa_flags = 0;
		int result = sigaction( SIGUSR1 , &sa , NULL);
		if(result == -1){
			perror("erreur sigaction\n");
			exit(-1);
		}
		wait();
	}
}
