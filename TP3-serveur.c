#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <ctype.h>
#define SEMDEBUG printf("[%d] Sem 0 : %d, Sem 1 : %d, Sem 2 : %d\n", __LINE__, semctl(semId, 0, GETVAL, 0), semctl(semId, 1, GETVAL, 0), semctl(semId, 2, GETVAL, 0));
#define MEMSIZE 11*sizeof(char)
#define CLEM 7777
#define CLES 7778

int memId ;//l'identifiant de la memoire partagee
int semId ;//l'identifiant du tableau de semaphore

int stopper(int retour){
	printf(" --Stop--\n");
	shmctl(memId, IPC_RMID, NULL);
	semctl( semId , 0, IPC_RMID, 0);
	exit(retour);
}

void sigintReact(){
	stopper(0);
}

int main(int argc, char *argv[]){
	//creation memoire partagee
	memId = shmget(CLEM, MEMSIZE, IPC_CREAT|0777);
	if(memId<0){
		perror("memId<0:");
		exit(-1);
	}
	//creation semaphores
	semId = semget(CLES, 3, IPC_CREAT|0777);
	if(semId<0){
		perror("semId<0:");
		shmctl(memId, IPC_RMID, NULL);
		exit(-1);
	}
	semctl( semId , 0, SETVAL, 1);
	semctl( semId , 1, SETVAL, 0);
	semctl( semId , 2, SETVAL, 0);
	// pour CTRL-c
	struct sigaction vsa;
	vsa.sa_handler = sigintReact;
	sigemptyset( &vsa.sa_mask );
	vsa.sa_flags = 0;
	int result = sigaction( SIGINT, &vsa , NULL);
	// corps du programme
	struct sembuf sb = {0,0,0};
	char *texte;
	texte = (char *)shmat(memId, NULL, SHM_R | SHM_W);
	printf("Serveur ON\n");
	while(1){
		//attendre que client qui ecrit ai fini
		sb.sem_num = 1;
		sb.sem_op = -1;
		result = semop( semId, &sb, 1);
		if(result == -1){
			perror("erreur serveur semop 2:");
			stopper(-1);
		}
		//traiter les donnees
		int i;
		for(i=0 ; i<MEMSIZE-1 ; i++){
			if(texte[i] == '\0') break;
			texte[i] = (char)toupper(texte[i]);
		}
		sleep(5);//pour simuler
		//permettre au client de lire
		sb.sem_num = 2;
		sb.sem_op = +1;
		result = semop( semId, &sb, 1);
		if(result == -1){
			perror("erreur serveur semop 3:");
			stopper(-1);
		}
	}
}
