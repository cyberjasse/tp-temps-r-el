#include <stdio.h>	// Necessaire pour printf()
#include <stdlib.h>	// Necessaire pour exit()

int main(int argc, char *argv[])
{
	void fils(int in, int out){
		int sum = 0;
		int result;
		int data = 0;
		while(1){ //bouh pas bien
			result = read(in , &data, sizeof(int));
			if(result < 0){
				printf("problem dans fils");
				exit(-1);
			}
			if( data != 0){
				sum = sum + data;
			}
			else{
				data = sum;
				result = write(out, &data, sizeof(int));
				exit(0);
			}
		}
	}

	//creation pipe pour fils gauche
	int fdtoG[2];
	int result = pipe(fdtoG);
	if(result<0){ exit(-1); }
	int fdfromG[2];
	result = pipe(fdfromG);
	if(result<0){ exit(-1); }
	//creation pipe pour fils droit
	int fdtoD[2];
	result = pipe(fdtoD);
	if(result<0){ exit(-1); }
	int fdfromD[2];
	result = pipe(fdfromD);
	if(result<0){ exit(-1); }
	//creation fils gauche
	int id = fork();
	if(id < 0){
		printf("problem premier fork\n");
		close(fdtoG[0]);
		close(fdtoG[1]);
		close(fdtoD[0]);
		close(fdtoD[1]);
		close(fdfromG[0]);
		close(fdfromG[1]);
		close(fdfromD[0]);
		close(fdfromD[1]);
		exit(-1);
	}
	else if(id == 0){//fils gauche
		fils(fdtoG[0] , fdfromG[1]);
	}
	//creation fils droit
	id = fork();
	if(id < 0){
		int data = 0;
		write(fdtoG[1], &data, sizeof(int) );
		printf("problem premier fork\n");
		close(fdtoG[0]);
		close(fdtoG[1]);
		close(fdtoD[0]);
		close(fdtoD[1]);
		close(fdfromG[0]);
		close(fdfromG[1]);
		close(fdfromD[0]);
		close(fdfromD[1]);
		wait(NULL);
		exit(-1);
	}
	else if(id == 0){//fils droit
		fils(fdtoD[0] , fdfromD[1]);
	}
	int data = -1;
	//saisie des donnees
	while( data != 0){
		printf("Entrez un entier\n");
		scanf("%d", &data);
		if( data > 0){
			result = write(fdtoG[1], &data, sizeof(int) );
		}
		else if( data < 0){
			result = write(fdtoD[1], &data, sizeof(int) );
		}
		else{
			result = write(fdtoG[1], &data, sizeof(int) );
			result = write(fdtoD[1], &data, sizeof(int) );
		}
	}
	//attente
	wait(NULL);
	wait(NULL);
	//lecture des resultats
	int dataG = 0;
	int dataD = 0;
	result = read(fdfromG[0], &dataG, sizeof(int) );
	result = read(fdfromD[0], &dataD, sizeof(int) );
	int sum = dataG + dataD;
	printf("La somme totale est %d\n", sum);
	//fermeture
	close(fdtoG[0]);
	close(fdtoG[1]);
	close(fdtoD[0]);
	close(fdtoD[1]);
	close(fdfromG[0]);
	close(fdfromG[1]);
	close(fdfromD[0]);
	close(fdfromD[1]);
	exit(0);
}
