#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/sem.h>
#include <sys/types.h>
#include <sys/ipc.h>

int semId;	// l'identifiant du tableau de semaphore
int in;		// le descripteur de fichier d'entree du tube
int out;	// le descripteur de fichier de sortie du tube

void stopper(){ // arrete presque proprement le processus
	printf("Arret presque propre\n");
	semctl( semId , 0, IPC_RMID, 0);
	exit(-1);
}

void fils( int numsem, int out){ // implementation de fils
	struct sembuf sb = { numsem, -1, 0};
	int data = 0;
	int sum = 0;
	while(1){
		//down
		int result = semop( semId, &sb, 1);
		if(result == -1){
			perror("erreur semop fils:");
			stopper();
		}
		//lecture
		result = read(out , &data, sizeof(int));
		if(result < 0){
			perror("erreur de lecture:");
			stopper();
		}
		//traitement
		sum = sum + data;
		//condition d'arret
		if( data == 0){
			printf("Je suis le fils %d ma somme est %d\n", numsem, sum);
			exit(0);
		}
	}
}

int main(int argc, char *argv[]){
	//creation semaphore
	semId = semget(IPC_PRIVATE, 2, 0777);
	semctl( semId , 0, SETVAL, 0);
	semctl( semId , 1, SETVAL, 0);
	
	// pour CTRL-c
	struct sigaction vsa;
	vsa.sa_handler = stopper;
	sigemptyset( &vsa.sa_mask );
	vsa.sa_flags = 0;
	int result = sigaction( SIGINT, &vsa , NULL);
	
	//initialisation du tube
	int p[2];
	result = pipe(p);
	if(result<0){
		perror("erreur init du tube:");
		stopper();
	}
	in = p[1];
	out = p[0];
	
	//corps du programme
	int id = fork();
	if( id < 0){
		perror("Erreur 1er fork:");
		stopper();
	}
	else if( id ==0){//fils de gauche
		fils(0, out);
	}
	else{
		int id = fork();
		if( id < 0){
			perror("Erreur 2e fork:");
			stopper();
		}
		else if( id ==0){//fils de gauche
			fils(1, out);
		}
		else{// pere
			int data = 1;
			struct sembuf sb = { 0, +1, 0};
			while( data != 0){
				printf("Entrez un entier\n");
				scanf("%d", &data);
				result = write(in, &data, sizeof(int) );
				if(result < 0){
					perror("erreur ecrire dans tube:");
					stopper();
				}
				if( data > 0){
					sb.sem_num = 1;
					result = semop( semId, &sb, 1);
					if(result == -1){
						perror("erreur semop pere1:");
						stopper();
					}
				}
				else if( data < 0){
					sb.sem_num = 0;
					result = semop( semId, &sb, 1);
					if(result == -1){
						perror("erreur semop pere2:");
						stopper();
					}
				}
				else{
					sb.sem_num = 0;
					result = semop( semId, &sb, 1);
					if(result == -1){
						perror("erreur semop pere1:");
						stopper();
					}
					result = write(in, &data, sizeof(int) );
					sb.sem_num = 1;
					result = semop( semId, &sb, 1);
					if(result == -1){
						perror("erreur semop pere2:");
						stopper();
					}
				}
			}
			wait(NULL);
			wait(NULL);
			close(in);
			close(out);
			stopper();
		}
	}
}
