#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
//Pas oublier gcc tp.c -­o tp ­-pthread

//DATA
int matrice[10][4]=	{8, 25, 3, 41, 
			11, 18, 3, 4,
			4, 15, 78, 12,
			1, 12, 0, 12, 
			7, 9, 13, 15,
			4, 21, 37, 89,
			1, 54, 7, 3, 
			15, 78, 7, 1,
			12, 15, 13, 47,
			91, 13, 72, 90};

int vecteur[4]=		{1, 2, 3, 4};

int resultat[10] = {0,0,0,0,0,0,0,0,0,0};

pthread_mutex_t mut;

//thread
void matvec(int *colonne){
	int k;
	int prod;
	int result;
	for(k=0 ; k<10 ; k++){
		prod = matrice[k][*colonne] * vecteur[*colonne];
		result = pthread_mutex_lock(&mut);
		resultat[k] = resultat[k] + prod;
		result = pthread_mutex_unlock(&mut);
	}
}
//
int main(int argc, char *argv[]){
	//on cree le mutex
	int result = pthread_mutex_init(&mut, NULL);
	//on cree et lance les threads
	pthread_t idTable[4];
	pthread_t tid = 0;
	int i;
	int colonnes[4];
	for(i=0 ; i<4 ; i++){
		colonnes[i] = i;
		result = pthread_create(&tid, NULL, (void *)matvec, &colonnes[i]);
		if(result != 0){
			perror("Erreur pthread_create:");
			exit(-1);
		}
		idTable[i] = tid;
	}
	//on attend la fin de tous les threads
	for(i=0 ; i<4 ; i++){
		pthread_join(idTable[i], NULL);
	}
	//on oublie pas de detruire le mutex
	result = pthread_mutex_destroy(&mut);
	//
	printf("resultat= ");
	for(i=0 ; i<10 ; i++){
		printf("%d ", resultat[i]);
	}
	printf("\n");
}
