#include <stdio.h>	// Necessaire pour printf()
#include <stdlib.h>	// Necessaire pour exit()
//#include <unistd.h>	// Necessaire pour execl()
//#include <signal.h>

int main(int argc, char *argv[])
{
	int id = fork();
	if( id == 0){//je suis P1
		id = fork();
		if( id == 0){// je suis p3
			printf("Je suis P3 le processus %d. Mon pere est %d\n", getpid(), getppid() );
			exit(0);
		}
		else if( id >0){// je suis p1
			printf("Je suis P1 le processus %d. Mon pere est %d\n", getpid(), getppid() );
			wait(NULL);
			exit(0);
		}
		else{
			printf("Problem dans P1, l'id que donne fork est = %d\n", id);
		}

	}
	else if( id > 0){//Je suis p0
		id = fork();
		if( id == 0){//encore p0
			printf("Je suis P0 le processus %d. Mon pere est %d\n", getpid(), getppid() );
			wait(NULL);
			wait(NULL);// ici on attend la fin de p1 et p2
			exit(0);
		}
		else if( id > 0){//je suis p2
			printf("Je suis P2 le processus %d. Mon pere est %d\n", getpid(), getppid() );
			exit(0);
		}
		else{
			printf("problem dans p0 a la tentative de creer p2: id = %d\n", id);
		}
	}
	else{
		printf("Problem: id = %d\n", id);
		exit(-1);
	}
}
