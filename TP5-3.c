#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#define FALSE 0
#define TRUE 1
//Pas oublier gcc tp.c -­o tp ­-pthread

pthread_t *tidTable;
pthread_mutex_t mut;
pthread_cond_t cond;
int condition = FALSE;
int numArg;

struct myStruct{
	char *word;
	pthread_t indexId;
};

void affiche(struct myStruct *ms){
	pthread_mutex_lock(&mut);
	if(condition == FALSE){// TOUJOURS ENTRE LOCK/UNLOCK
		pthread_cond_wait(&cond, &mut);
	}
	pthread_mutex_unlock(&mut);
	if(ms->indexId != numArg) pthread_join(tidTable[ms->indexId], NULL);
	printf("%s\n", ms->word);
}

int main(int argc, char *argv[]){
	if(argc < 2){
		printf("Il faut au moins 1 argument\n");
		exit(-1);
	}
	numArg = argc-1;
	//cree la condition pour que les thread demarrent que quand tidTable est rempli
	pthread_mutex_init(&mut, NULL);
	pthread_cond_init(&cond, NULL);
	//on cree et lance les threads
	pthread_t tid = 0;
	pthread_t idTable[numArg];
	int i;
	struct myStruct param[numArg];
	for(i=0 ; i<numArg ; i++){
		struct myStruct ms;
		ms.word = argv[i+1];
		ms.indexId = i+1;
		printf("le thread %d doit attendre le thread %ld\n", i, ms.indexId);
		param[i] = ms;
		int result = pthread_create(&tid, NULL, (void *)affiche, &param[i]);
		if(result != 0){
			perror("Erreur pthread_create:");
			exit(-1);
		}
		idTable[i] = tid;
	}
	tidTable = idTable;
	//le tableau est rempli, on demarre tout
	pthread_mutex_lock(&mut);
	condition = TRUE; // TOUJOURS ENTRE LOCK/UNLOCK
	pthread_cond_broadcast(&cond);
	pthread_mutex_unlock(&mut);
	//on attend la fin du dernier thread
	pthread_join(idTable[0], NULL);
	//detruire mutex et condition
	pthread_mutex_destroy(&mut);
	pthread_cond_destroy(&cond);
}
