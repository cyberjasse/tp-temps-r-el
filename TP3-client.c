#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/shm.h>
#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <ctype.h>
#define SEMDEBUG printf("[%d] Sem 0 : %d, Sem 1 : %d, Sem 2 : %d\n", __LINE__, semctl(semId, 0, GETVAL, 0), semctl(semId, 1, GETVAL, 0), semctl(semId, 2, GETVAL, 0));
#define MEMSIZE 11*sizeof(char)
#define CLEM 7777
#define CLES 7778

int memId ;//l'identifiant de la memoire partagee
int semId ;//l'identifiant du tableau de semaphore

int main(int argc, char *argv[]){
	// recherche memoire partagee
	memId = shmget(CLEM, MEMSIZE, 0777);
	if(memId<0){
		perror("client memId<0:");
		exit(-1);
	}
	// recherche semaphores
	semId = semget(CLES, 3, 0777);
	if(semId<0){
		perror("client semId<0:");
		exit(-1);
	}
	// corp du programme
	char texte[11];
	char *memoire;
	int result;
	struct sembuf sb = {0,0,0};
	while(1){
		//entree au clavier
		printf("Entrez un mot de maximum 10 lettres\n");
		scanf("%s", texte);
		texte[10] = '\0';//par mefiance de l'utilisateur
		//attendre droit a l'ecriture
		sb.sem_num = 0;
		sb.sem_op = -1;
		result = semop( semId, &sb, 1);
		if(result == -1){
			perror("erreur client semop 1:");
			exit(-1);
		}
		//ecriture
		memoire = (char *)shmat(memId, NULL, SHM_R | SHM_W);
		strcpy(memoire , texte);
		shmdt( (void *)memoire );
		//indiquer a serveur la fin de l'ecriture
		sb.sem_num = 1;
		sb.sem_op = +1;
		result = semop( semId, &sb, 1);
		if(result == -1){
			perror("erreur client semop 2:");
			exit(-1);
		}
		//attendre la fin traitement pour lire
		sb.sem_num = 2;
		sb.sem_op = -1;
		result = semop( semId, &sb, 1);
		if(result == -1){
			perror("erreur client semop 3:");
			exit(-1);
		}
		//lecture
		memoire = (char *)shmat(memId, NULL, SHM_R | SHM_W);
		printf("Resultat: %s\n", memoire);
		//permettre un client d'ecrire
		sb.sem_num = 0;
		sb.sem_op = +1;
		result = semop( semId, &sb, 1);
		if(result == -1){
			perror("erreur serveur semop 1:");
			exit(-1);
		}
		shmdt( (void *)memoire );
	}
}
